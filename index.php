<?
	$page_title = 'Icon Resize';

	require_once 'header.php';
?>

    <h1>Click or Drag Image Over Icon to Convert Icons</h1>

    <div id="apple-file-uploader" class="dropzone">
        <div id="apple">Apple icons</div>
    </div>

    <div id="facebook-file-uploader" class="dropzone">
        <div id="facebook">Facebook Icons</div>
    </div>

    <script>
        $(function() {
            $('#apple-file-uploader').dropzone({
                url: 'ajax-upload.php',
                sending: function(file, xhr, formData) {
                    $('#apple').hide();
                    formData.append('type', 'apple');
                },
                complete: function(data) {
                    $('body').append(data.xhr.response);
                }
            });

            $('#facebook-file-uploader').dropzone({
                url: 'ajax-upload.php',
                sending: function(file, xhr, formData) {
                    $('#facebook').hide();
                    formData.append('type', 'facebook');
                },
                complete: function(data) {
                    $('body').append(data.xhr.response);
                }
            });
        });
    </script>

<?
	require_once 'footer.php';
?>