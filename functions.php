<?php
	require_once 'config.php';
    require_once 'wideimage/WideImage.php';

    function create_zip_file($files = array(), $download_name)
    {
        $zip = new ZipArchive;
        $zip->open($download_name, ZipArchive::CREATE);

        foreach($files as $file)
        {
            $zip->addFile($file);
        }

        $zip->close();
    }

    function make_apple_icons($image_path)
    {
        $files_to_zip = array();

        $path = dirname(__FILE__) . '/';
        $parts = explode('.', $image_path);
        $extension = strtolower(end($parts));
        $icon_sizes = array(152, 144, 114, 80, 76, 72, 57, 50, 40);

        foreach($icon_sizes as $size)
        {
            $image = WideImage::load($image_path);
            $name = $size . 'x' . $size . '.' . $extension;

            $image->resize($size, $size)->saveToFile($path . $name);

            $files_to_zip[] = $name;
        }

        create_zip_file($files_to_zip, 'apple-icons.zip');

        foreach($files_to_zip as $file)
        {
            unlink($file);
        }

        echo "<script>window.location.href = 'apple-download.php';</script>";
    }

    function make_facebook_icons($image_path)
    {
        $files_to_zip = array();

        $path = dirname(__FILE__) . '/';
        $parts = explode('.', $image_path);
        $extension = strtolower(end($parts));
        $icon_sizes = array(
            '0' => array(
                'width' => 1024,
                'height' => 1024
            ),
            '1' => array(
                'width' => 800,
                'height' => 150
            ),
            '2' => array(
                'width' => 394,
                'height' => 150
            ),
            '3' => array(
                'width' => 180,
                'height' => 115
            ),
            '4' => array(
                'width' => 155,
                'height' => 100
            ),
            '5' => array(
                'width' => 16,
                'height' => 16
            ),
        );

        foreach($icon_sizes as $size)
        {
            $image = WideImage::load($image_path);
            $name = $size['width'] . 'x' . $size['height'] . '.' . $extension;

            $image->resize($size['width'], $size['height'], 'outside')->crop('center', 'middle', $size['width'], $size['height'])->saveToFile($path . $name);

            $files_to_zip[] = $name;
        }

        create_zip_file($files_to_zip, 'facebook-icons.zip');

        foreach($files_to_zip as $file)
        {
            unlink($file);
        }

        echo "<script>window.location.href = 'facebook-download.php';</script>";
    }