<?php
    require_once 'functions.php';

    $type = isset($_POST['type']) ? $_POST['type'] : null;
    $temp_file = $_FILES['file']['tmp_name'];
    $target_path = dirname(__FILE__) . '/uploads/';
    $parts = explode('.', $_FILES['file']['name']);
    $extension = end($parts);
    $file_name = rand(10000, 1000000) . '-' . hash('sha1', $_FILES['file']['name']) . '.' . $extension;
    $new_file = $target_path . $file_name;

    if(move_uploaded_file($temp_file, $new_file)) {
        if($type === 'apple') {
            make_apple_icons($new_file);
        } elseif($type === 'facebook') {
            make_facebook_icons($new_file);
        } else {
            echo 'This script only deals with Apple and Facebook POST data';
        }
    } else {
        echo 'There was an error uploading the file, check the permissions of uploads/';
    }