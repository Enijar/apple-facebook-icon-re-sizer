<?
	session_start();
	
	require_once 'functions.php';
?>
<!DOCTYPE html>

<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no">
        <meta name="description" content="Invite your friends, meet new people, discuss anything!
        Chat until your heart's content and your fingers burn.">
	
		<title><? echo $page_title; ?></title>
		<link rel="stylesheet" href="<? echo $config['site']['root']; ?>/css/global.css">
        <link rel="stylesheet" href="<? echo $config['site']['root']; ?>/css/dropzone.css">
		<script src="<? echo $config['site']['root']; ?>/js/jquery.min.js"></script>
        <script src="<? echo $config['site']['root']; ?>/js/dropzone.js"></script>
	</head>
	
	<body>
	
		<div id="wrapper">
			<div id="container" class="clearfix">