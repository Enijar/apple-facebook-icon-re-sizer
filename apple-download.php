<?php
    header('Content-Type: application/zip');
    header('Content-disposition: attachment; filename=apple-icons.zip');
    header('Content-Length: ' . filesize('apple-icons.zip'));
    readfile('apple-icons.zip');
    header('Location: ./');